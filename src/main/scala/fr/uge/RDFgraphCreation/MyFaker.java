package fr.uge.RDFgraphCreation;

import com.github.javafaker.Faker;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class MyFaker {
    Faker faker = new Faker(new Locale("en-US"));

    public String getLastName(){
        return faker.name().lastName();
    }
    public String getFirstName(){
        return faker.name().firstName();
    }
    public String getGender(){
        return faker.dog().gender();
    }
    public String getZipCode(){
        return faker.address().zipCode();
    }
    public Date getDateOfBirthStudent(){
        return faker.date().birthday(20,30);
    }
    public Date getDateOfBirthProfessor(){
        return faker.date().birthday(30,70);
    }
    public String getVaccinName(){
        int n = faker.number().numberBetween(1,5);
        switch (n){
            case 1: return "Pfizer";
            case 2: return "Moderna";
            case 3: return "AstraZeneca";
            case 4: return "SpoutnikV";
            default: return "CanSinoBio";
        }
    }
    public Date getVaccinDate(){
        return faker.date().past(400, TimeUnit.DAYS);
    }
}
