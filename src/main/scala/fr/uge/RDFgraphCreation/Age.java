package fr.uge.RDFgraphCreation;

public class Age {
    public int id;
    public int age;

    public Age(int id, int age) {
        this.id = id;
        this.age = age;
    }
}
