package fr.uge.RDFgraphCreation

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.apache.jena.ontology.OntModelSpec
import org.apache.jena.rdf.model.{ModelFactory, Resource}

import java.io.File
import java.util.Date
import scala.collection.mutable.ListBuffer
import scala.util.Random

object ExtendRDF extends App {
  val faker = new MyFaker()
  val model = ModelFactory.createDefaultModel()
  val producer = new MyProducer()

  sealed trait SideEffect {
    val SIDER_code : String
    val name : String
  }

  case object Injection_site_pain extends SideEffect{ override val SIDER_code: String = "C0151828"; override val name: String = "Injection site pain"}
  case object fatigue extends SideEffect{ override val SIDER_code: String = "C0015672"; override val name: String = "fatigue"}
  case object headache extends SideEffect{ override val SIDER_code: String = "C0018681"; override val name: String = "headache"}
  case object Muscle_pain extends SideEffect{ override val SIDER_code: String = "C0231528"; override val name: String = "Muscle pain"}
  case object chills extends SideEffect{ override val SIDER_code: String = "C0085593"; override val name: String = "chills"}
  case object Joint_pain extends SideEffect{ override val SIDER_code: String = "C0003862"; override val name: String = "Joint pain"}
  case object fever extends SideEffect{ override val SIDER_code: String = "C0015967"; override val name: String = "fever"}
  case object Injection_site_swelling extends SideEffect{ override val SIDER_code: String = "C0151605"; override val name: String = "Injection site swelling"}
  case object Injection_site_redness extends SideEffect{ override val SIDER_code: String = "C0852625"; override val name: String = "Injection site redness"}
  case object Nausea extends SideEffect{ override val SIDER_code: String = "C0027497"; override val name: String = "Nausea"}
  case object Malaise extends SideEffect{ override val SIDER_code: String = "C0231218"; override val name: String = "Malaise"}
  case object Lymphadenopathy extends SideEffect{ override val SIDER_code: String = "C0497156"; override val name: String = "Lymphadenopathy"}
  case object Injection_site_tenderness extends SideEffect{ override val SIDER_code: String = "C0863083"; override val name: String = "Injection site tenderness"}


  case class Person_Vaccinated(date:String, id:String,fname:String,lname:String,vname:String,side_effect:String,SIDER_code:String)

  def addIdentifier(s: Resource, id: Int): Unit = {
    val identifier = model.createProperty("http://UGE/Projet/Person/identifier")
    val statement = model.createLiteralStatement(s,identifier, id)
    model.add(statement)
  }
  def addLname(s: Resource): Unit = {
    val lname = model.createProperty("http://UGE/Projet/Person/lname")
    val statement = model.createLiteralStatement(s, lname, faker.getLastName)
    model.add(statement)
  }

  def addFname(s: Resource): Unit = {
    val fname = model.createProperty("http://UGE/Projet/Person/fname")
    val statement = model.createLiteralStatement(s,fname, faker.getFirstName)
    model.add(statement)
  }

  def addGender(s: Resource): Unit = {
    val gender = model.createProperty("http://UGE/Projet/Person/gender")
    val statement = model.createLiteralStatement(s,gender, faker.getGender)
    model.add(statement)
  }

  def addZipCode(s: Resource): Unit = {
    val zipCode = model.createProperty("http://UGE/Projet/Person/ZipCode")
    val statement = model.createLiteralStatement(s,zipCode, faker.getZipCode)
    model.add(statement)
  }

  def addDateOfBirth(s: Resource): Unit = {
    val dateOfBirth = model.createProperty("http://UGE/Projet/Person/dateOfBirth")
    if(s.hasProperty(model.createProperty("http://swat.cse.lehigh.edu/onto/univ-bench.owl#teacherOf"))) {
      val statement = model.createLiteralStatement(s,dateOfBirth, faker.getDateOfBirthProfessor)
      model.add(statement)
    } else {
      val statement = model.createLiteralStatement(s,dateOfBirth, faker.getDateOfBirthStudent)
      model.add(statement)
    }
  }

  def AddForPerson(s: Resource, id: Int): Unit = {
    addIdentifier(s, id)
    addLname(s)
    addFname(s)
    addGender(s)
    addZipCode(s)
    addDateOfBirth(s)
  }

  def addVaccinToPersons(vaccinM : Integer=48, vaccinF : Integer=52): Unit ={
    val male = listAllMale()
    val female = listAllFemale()
    val nbMale = (male.size() * vaccinM) / 100
    val nbFemale = (female.size() * vaccinF) / 100
    var count = 0
    male.forEach(person => {
      if(count < nbMale){
        AddVaccin(person)
        count += 1
      }
    })
    count = 0
    female.forEach(person => {
      if(count < nbFemale) {
        AddVaccin(person)
        count += 1
      }
    })
  }

  def AddVaccin(s: Resource): Unit = {
    val propertyVaccin= model.createProperty("http://UGE/Projet/Person/Vaccinated")
    val objectvaccin =model.createResource(s.toString + "#vaccin")
    val vaccinStmt = model.createStatement(s, propertyVaccin, objectvaccin)
    val propertyVaccinName=model.createProperty("http://UGE/Projet/Person/vaccinName")
    val vnameStmt = model.createLiteralStatement(objectvaccin, propertyVaccinName, faker.getVaccinName)
    val propertyVaccinDate= model.createProperty("http://UGE/Projet/Person/vaccinDate")
    val dateStmt = model.createLiteralStatement(objectvaccin, propertyVaccinDate, faker.getVaccinDate)
    model.add(vaccinStmt)
    model.add(vnameStmt)
    model.add(dateStmt)
  }

  def listAllTypes() = {
    val inf = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM_MICRO_RULE_INF)
    inf.read("file:univ-bench.owl")
    val pers = inf.getOntClass("http://swat.cse.lehigh.edu/onto/univ-bench.owl#Person")
    val it = pers.listSubClasses(false)
    it.filterDrop(s => s.toString.contains("http://www.w3.org/2002/07/owl#Nothing"))
  }

  def listAllMale() = {
    val rdfType = model.createProperty("http://UGE/Projet/Person/gender")
    val it = model.listSubjectsWithProperty(rdfType, "male")
    it.toList
  }
  def listAllFemale() = {
    val rdfType = model.createProperty("http://UGE/Projet/Person/gender")
    val it = model.listSubjectsWithProperty(rdfType, "female")
    it.toList
  }
  def listAllVaccinated() = {
    val rdfType = model.createProperty("http://UGE/Projet/Person/Vaccinated")
    val it = model.listSubjectsWithProperty(rdfType)
    it.toList
  }

  def generate_side_effect(): SideEffect ={
    val rand = new Random()
    rand.nextInt(13) match {
      case 0 => Injection_site_pain
      case 1 => fatigue
      case 2 => headache
      case 3 => Muscle_pain
      case 4 => chills
      case 5 => Joint_pain
      case 6 => fever
      case 7 => Injection_site_swelling
      case 8 => Injection_site_redness
      case 9 => Nausea
      case 10 => Malaise
      case 11 => Lymphadenopathy
      case 12 => Injection_site_tenderness

    }
  }

  def VaccinatedJson(): Unit = {
    val mapper = new ObjectMapper()
    mapper.registerModule(DefaultScalaModule)
    val vaccinated = listAllVaccinated()
    val propertyId = model.createProperty("http://UGE/Projet/Person/identifier")
    val propertyFname = model.createProperty("http://UGE/Projet/Person/fname")
    val propertyLname = model.createProperty("http://UGE/Projet/Person/lname")
    val propertyVaccin= model.createProperty("http://UGE/Projet/Person/Vaccinated")
    val propertyVaccinName= model.createProperty("http://UGE/Projet/Person/vaccinName")
    val propertyVaccinDate= model.createProperty("http://UGE/Projet/Person/vaccinDate")
    val ListVaccinated = new ListBuffer[Person_Vaccinated]()
    vaccinated.forEach(person => {
      val id = model.getProperty(person,propertyId).getString
      val fname = model.getProperty(person,propertyFname).getString
      val lname = model.getProperty(person,propertyLname).getString
      val vaccin = model.getProperty(person,propertyVaccin).getResource
      val vname = model.getProperty(vaccin,propertyVaccinName).getString
      val vdate = model.getProperty(vaccin,propertyVaccinDate).getString
      val side_effect = generate_side_effect()
      ListVaccinated += Person_Vaccinated(vdate,id,fname,lname,vname,side_effect.name,side_effect.SIDER_code)
    })
    mapper.writeValue(new File("PersonVaccinated.json"), ListVaccinated)
  }

  def AddAgeToTopic(): Unit = {
    val vaccinated = listAllVaccinated()
    val propertyId = model.createProperty("http://UGE/Projet/Person/identifier")
    val propertyDateOfBirth = model.createProperty("http://UGE/Projet/Person/dateOfBirth")
    vaccinated.forEach(person => {
      val mapper = new ObjectMapper()
      val id = model.getProperty(person,propertyId).getString
      val dateOfBirth : Date = model.getProperty(person,propertyDateOfBirth).getResource.as(Class[Date])
      val date = new Date().setTime()
      val year = new Date()
      year.setTime(System.currentTimeMillis() - dateOfBirth.getTime)
      val age = new Age(id.toInt, year.getYear)
      val jsonString = mapper.writeValueAsString(age)
      producer.send(jsonString)
    })
  }

  model.read("file:lubm1.ttl","TTL")
  val list = listAllTypes().toList
  val rdfType = model.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
  var id = 0

  list.forEach( o => {
    model.listSubjectsWithProperty(rdfType,o).forEach( s => {
      AddForPerson(s,id)
      id += 1
    })
  })
  addVaccinToPersons(50,50)
  VaccinatedJson()

}
