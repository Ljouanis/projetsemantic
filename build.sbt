name := "Projet"

version := "0.1"

scalaVersion := "2.13.8"

libraryDependencies += "org.apache.jena" % "jena-core" % "4.3.2"
libraryDependencies += "com.github.javafaker" % "javafaker" % "1.0.2"
libraryDependencies += "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.11.2"
libraryDependencies += "org.apache.kafka" % "kafka-clients" % "3.1.0"