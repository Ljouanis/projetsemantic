Pour le compte rendu, je vais expliquer à quoi correspond chaque fichier.

MyFaker est ma classe écrite en java qui utilise la bibliothèque Faker pour récupérer des informations gérérés aléatoirement à patir de l'objet faker qui a été fabriqué avec l'option Locale "en-Us" pour obtenir des informations en format us pour l'adresse.
Ce qui nous intérésse à obtenir est le LastName, le FirstName, le genre, le ZipCode, la date de naissance d'un étudiant ou d'un professeur, la date du dernier vaccin et le nom du vaccin.

ExtendRDF est ma classe main écrite en scala qui lance le projet, c'est ici où l'on rajoute les données dans le model.
Pour obtenir les données de base, j'ouvre le fichier lubm1.ttl dans le model puis je récupère l'ontologie univ-bench.owl pour pouvoir récupérer derrière les types qui correspondent à une personne. Puis pour chaque type de personne, je récupère chaque individu de cette catégorie et je rajoute les informations aux individus à partir de propriété que j'ai créé et les données gérérés par ma classe MyFaker. Ensuite, une fois que chaque personne à leurs informations rajoutés, j'ai une fonction qui prend tous les individus males d'un côtés et les femelles de l'autre pour leur rajouter le vaccin à partir d'un pourcentage donné à l'appel de fonction ou une valeur de 48% pour les males et 52% pour les femelles. Enfin, une fois les données de vaccin rajoutés, j'appelle une fonction qui va créer un fichier json appeler PersonVaccinated.json où se trouve chaque personne vacciné avec les données de la case class Person_Vaccinated qui comprend la date du vaccin, l'id de la personne, le first name et last name, le nom du vaccin, l'effet secondaire eu avec le vaccin et le Sider Code de l'eefet secondaire.

Age est la classe pour les information de l'age à mettre dans le topic.

MyConsumer et MyProducer sont les classes qui créer un kafka producer/consumer pour mon projet qui utilise le topic age pour pouvoir lire les informations de type age.

Sinon, je n'ai pas fini ce qui restait à faire car je n'ai pas compris les fonctionnalités des kafkas streams et je me suis arrêté à la première question du fichier fin du projet.



